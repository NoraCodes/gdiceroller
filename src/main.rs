extern crate gtk;
extern crate glib;

use std;
use std::rc::Rc;
use std::cell::RefCell;
use gtk::prelude::*;

mod rolls;
use rolls::roll_expression;
mod state;
use state::State;
mod main_window;
use main_window::MainWindow;

fn main() {
    // Start up the GTK3 subsystem.
    gtk::init().expect("Unable to start GTK3. Error");

    // Create the main window.
    let gui = Rc::new(MainWindow::new()); 

    // Set up the application state.
    let state = Rc::new(RefCell::new(State::new()));

    // Add callbacks for the buttons.
    for sides in &[4, 6, 8, 10, 12, 20, 100] {
        let spec = format!("1d{}", sides);
        let button = gui.button(&format!("rollD{}", sides));
        let gui = gui.clone();
        let state = state.clone();
        button.connect_clicked(move |_| {
            let mut state = state.borrow_mut();
            state.update_from_roll_result(roll_expression(&spec));
            gui.update_from(&state);
        });
    }

    {
        let button = gui.button("clearResult");
        let gui = Rc::clone(&gui);
        let state = Rc::clone(&state);
        button.connect_clicked(move |_| {
            let mut state = state.borrow_mut();
            state.value = 0;
            gui.update_from(&state);
        });
    }
    {
        let button = gui.button("halveDownResult");
        let gui = Rc::clone(&gui);
        let state = Rc::clone(&state);
        button.connect_clicked(move |_| {
            let mut state = state.borrow_mut();
            let prev_value = state.value;
            state.value = (f64::from(prev_value) / 2.0).floor() as i32;
            gui.update_from(&state);
        });
    }
    {
        let button = gui.button("halveUpResult");
        let gui = Rc::clone(&gui);
        let state = Rc::clone(&state);
        button.connect_clicked(move |_| {
            let mut state = state.borrow_mut();
            let prev_value = state.value;
            state.value = (f64::from(prev_value) / 2.0).ceil() as i32;
            gui.update_from(&state);
        });
    }

    {
        let button = gui.button("rollUser");
        let gui = Rc::clone(&gui);
        let state = Rc::clone(&state);
        button.connect_clicked(move |_| {
            let spec = gui.user_spec_entry().get_text().unwrap().to_string();

            let mut state = state.borrow_mut();
            state.update_from_roll_result(roll_expression(&spec));
            gui.update_from(&state);
        });
    }

    {
        let user_spec_entry = gui.user_spec_entry();
        let gui = Rc::clone(&gui);
        let state = Rc::clone(&state);
        user_spec_entry.connect_activate(move |entry| {
            let spec = entry.get_text().unwrap().to_string();

            let mut state = state.borrow_mut();
            state.update_from_roll_result(roll_expression(&spec));
            gui.update_from(&state);
        });
    }

    gui.start();
    gtk::main();
}

